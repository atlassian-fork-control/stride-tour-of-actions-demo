// init project
const express = require("express");
const app = express();
const bodyParser = require("body-parser");

//The Stride JS client library handles API authentication and calling REST endpoints from the Stride API
const { CLIENT_SECRET } = process.env;
const stride = require("./strideClient");

//Middleware
const auth = require("./middleware/auth")(CLIENT_SECRET);
const tutorialMiddleware = require("./middleware/tutorialEndpoints");
const lifecycle = require("./middleware/lifecycle");

let botUser = null;

//Caches the bot user info for use when parsing direct messages
async function getBotsUser() {
  if (!botUser) {
    botUser = await stride.api.users.me().catch(err => {
      console.error(`Bot user lookup failed.`);
      throw err;
    });
  }
  return botUser;
}

app.use(bodyParser.json());
app.use(tutorialMiddleware);
app.use(lifecycle);

app.post("/action-success", auth, async function(req, res, next) {
  res.json({
    message: `🎉 ${new Date().toTimeString()}`
  });

  console.log(
    "\nReceived actionTarget-callService-success webhook. Message body:\n",
    JSON.stringify(req.body, null, 2)
  );
  const { cloudId, conversationId } = res.locals.context;

  await require("./messages/serviceActionHit")(cloudId, conversationId, req.body);
});

app.post("/action-nextAction", auth, async function(req, res, next) {
  res.json({
    nextAction: {
      target: {
        key: "actionTarget-openDialog"
      },
      parameters: {
        nextAction: "was used!"
      }
    }
  });

  console.log(
    "\nReceived actionTarget-callService-nextAction webhook. Message body:\n",
    JSON.stringify(req.body, null, 2)
  );
  const { cloudId, conversationId } = res.locals.context;

  await require("./messages/serviceActionHit")(cloudId, conversationId, req.body);
});
app.post("/action-inlineMessageSelect", auth, async function(req, res, next) {
  const { cloudId, conversationId } = res.locals.context;

  if (req.body.parameters) {
    switch (req.body.parameters.selectedValue) {
      case "openSidebar":
        res.json({
          nextAction: {
            target: {
              key: "actionTarget-openSidebar"
            }
          }
        });
        break;
      case "openDialog":
        res.json({
          nextAction: {
            target: {
              key: "actionTarget-openDialog"
            }
          }
        });
        break;
      case "openRoom":
        let opts = {
          body: {
            name: "Stride - Tour of Actions test room" + new Date().getTime(),
            privacy: "public",
            topic: "This room was created via the API"
          }
        };

        let createdRoom = await stride.api.conversations.create(cloudId, opts);

        res.json({
          nextAction: {
            target: {
              receiver: "stride",
              key: "openConversation"
            },
            parameters: {
              conversationId: createdRoom.id
            }
          }
        });
        break;
      case "openHighlights":
        res.json({
          nextAction: {
            target: {
              receiver: "stride",
              key: "openHighlights"
            }
          }
        });
        break;
      case "openFiles":
        res.json({
          nextAction: {
            target: {
              receiver: "stride",
              key: "openFilesAndLinks"
            }
          }
        });
        break;
      default:
        res.status(400).json({ error: `😿 Unknown action '${req.body.parameters.selectedValue}'` });
        break;
    }
  }

  console.log("\nReceived actionTarget-inlineSelect webhook. Message body:\n", JSON.stringify(req.body, null, 2));

  await require("./messages/serviceActionHit")(cloudId, conversationId, req.body);
});

app.post("/action-failure", auth, async function(req, res, next) {
  res.status(401).json({
    error: `😿 ${new Date().toTimeString()}`
  });

  console.log(
    "\nReceived actionTarget-callService-failure webhook. Message body:\n",
    JSON.stringify(req.body, null, 2)
  );
  const { cloudId, conversationId } = res.locals.context;

  await require("./messages/serviceActionHit")(cloudId, conversationId, req.body);
});

app.post(
  "/mentions",
  auth,
  async (req, res, next) => {
    //for webhooks send the response asap or the messages will get replayed up to 3 times
    res.sendStatus(204);
    next();
  },
  showMessageCommands
);

app.post("/external-page-call", auth, async (req, res, next) => {
  let message = req.body.message;
  const { cloudId, conversationId, userId } = res.locals.context;
  console.log(JSON.stringify(res.locals.context, null, 2), message);

  await require("./messages/externalPage")(cloudId, conversationId, message, userId);

  res.sendStatus(204);
});

async function showMessageCommands(req, res, next) {
  console.log("sending message with actions");

  //The auth middleware stores the decrypted JWT token values as res.locals.context
  //The cloudId and conversationId are needed to send a message back
  const { cloudId, conversationId } = res.locals.context;
  await require("./messages/messageWithActions")(cloudId, conversationId);
}

/**
 *  @see {@link https://developer.atlassian.com/cloud/stride/apis/modules/chat/bot-messages | API Reference: Bot messages }
 *  @see {@link https://developer.atlassian.com/cloud/stride/learning/bots/ | Concept Guide }
 *  @see {@link https://developer.atlassian.com/cloud/stride/learning/adding-bots | How-to Guide }
 *  @description
 *  Receives all direct messages and passes them onto the processing middleware
 **/
app.post(
  "/direct",
  auth,
  async (req, res, next) => {
    //for webhooks send the response asap or the messages will get replayed up to 3 times
    res.sendStatus(204);

    let mentions = stride.utils.getMentionIdsFromADF(req.body.message);

    if (mentions.includes((await getBotsUser()).account_id)) {
      console.log("direct message received with @ mention. Ignoring to prevent duplicate responses");
      return;
    }

    next();
  },
  showMessageCommands
);

// listen for requests :)
const listener = app.listen(process.env.PORT, function() {
  console.log("Your app is listening on port " + listener.address().port);
});
